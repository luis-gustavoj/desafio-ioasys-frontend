import { setCookie, parseCookies, destroyCookie } from "nookies";
import Router from "next/router";

import { api } from "../services/api";
import { createContext, ReactNode, useEffect, useState } from "react";

type User = {
  id: string;
  name: string;
  gender: string;
};

type SignInCredentials = {
  email: string;
  password: string;
};

type AuthContextData = {
  signIn: (credentials: SignInCredentials) => Promise<void> | string;
  signOut: () => void;
  user: User;
  isAuthenticated: boolean;
};

type AuthProviderProps = {
  children: ReactNode;
};

export const AuthContext = createContext({} as AuthContextData);

export function AuthProvider({ children }: AuthProviderProps) {
  const [user, setUser] = useState<User>(null);
  const isAuthenticated = !!user;

  async function signIn({ email, password }: SignInCredentials) {
    try {
      const response = await api.post("auth/sign-in", {
        email,
        password,
      });

      const { id, name, gender } = response.data;
      const { authorization, "refresh-token": refreshToken } = response.headers;

      setCookie(undefined, "auth.token", authorization, {
        maxAge: 60 * 60 * 24 * 30, // 30 days
        path: "/",
      });

      setCookie(undefined, "auth.refreshToken", refreshToken, {
        maxAge: 60 * 60 * 24 * 30, // 30 days
        path: "/",
      });

      setUser({ id, name, gender });

      setCookie(undefined, "user.data", JSON.stringify({ id, name, gender }), {
        maxAge: 60 * 60 * 24 * 30, // 30 days
        path: "/",
      });

      Router.push("/books");
    } catch (err) {
      if (err.response) {
        const { message } = err.response.data.errors;
        return message;
      }
    }
  }

  function signOut() {
    destroyCookie(undefined, "auth.token");
    destroyCookie(undefined, "auth.refreshToken");
    destroyCookie(undefined, "user.data");

    Router.push("/");
  }

  return (
    <AuthContext.Provider value={{ signIn, signOut, isAuthenticated, user }}>
      {children}
    </AuthContext.Provider>
  );
}
