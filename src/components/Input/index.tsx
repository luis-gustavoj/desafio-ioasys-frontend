import { InputHTMLAttributes } from "react";

import styles from "./styles.module.scss";

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  label?: string;
}

export const Input = ({ name, label, children, ...rest }: InputProps) => {
  return (
    <div className={styles.inputWrapper}>
      <label htmlFor={name}>{label}</label>
      <input name={name} {...rest}>
        {children}
      </input>
    </div>
  );
};
