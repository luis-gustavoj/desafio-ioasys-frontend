import { useEffect } from "react";
import styles from "./styles.module.scss";

interface BookCardProps {
  id: string;
  title: string;
  authors: string[];
  pageCount: string;
  publisher: string;
  publishedDate: string;
  imgUrl: string;
  onClick: (id: string) => void;
}

export const BookCard = ({
  id,
  title,
  authors,
  pageCount,
  publisher,
  publishedDate,
  imgUrl,
  onClick,
}: BookCardProps) => {
  return (
    <div className={styles.bookContainerWrapper} onClick={() => onClick(id)}>
      <div className={styles.bookImageWrapper}>
        <img
          src="https://d2drtqy2ezsot0.cloudfront.net/Book-3.jpg"
          alt="book image"
        />
      </div>
      <div className={styles.colTwo}>
        <div className={styles.headingWrapper}>
          <p className={styles.title}>{title}</p>
          {authors?.map((author, index) => {
            return (
              <p key={index} className={styles.author}>
                {author}
              </p>
            );
          })}
        </div>
        <div className={styles.infoWrapper}>
          <p className={styles.pages}>{pageCount} páginas</p>
          <p className={styles.publisher}>{publisher}</p>
          <p className={styles.date}>{publishedDate}</p>
        </div>
      </div>
    </div>
  );
};
