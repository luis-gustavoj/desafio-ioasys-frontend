import { Dispatch, SetStateAction } from "react";
import styles from "./styles.module.scss";

interface PaginationProps {
  currentPage: number;
  totalCount: number;
  onChange: Dispatch<SetStateAction<number>>;
}

export const Pagination = ({
  currentPage,
  totalCount,
  onChange,
}: PaginationProps) => {
  return (
    <div className={styles.pagination}>
      <p>
        Página <b>{currentPage}</b> de <b>{totalCount}</b>
      </p>
      <div>
        <button
          type="button"
          onClick={() => onChange(currentPage - 1)}
          disabled={currentPage === 1}
        >
          <BackArrow />
        </button>
        <button
          type="button"
          onClick={() => onChange(currentPage + 1)}
          disabled={currentPage === totalCount}
        >
          <NextArrow />
        </button>
      </div>
    </div>
  );
};

const BackArrow = () => {
  return (
    <svg
      width="6"
      height="10"
      viewBox="0 0 6 10"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M5 1L1 5L5 9"
        stroke="currentColor"
        strokeOpacity="0.4"
        strokeLinejoin="round"
      />
    </svg>
  );
};

const NextArrow = () => {
  return (
    <svg
      width="6"
      height="10"
      viewBox="0 0 6 10"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M1 1L5 5L1 9" stroke="currentColor" strokeLinejoin="round" />
    </svg>
  );
};
