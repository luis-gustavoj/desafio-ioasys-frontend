import { BookModal } from "../Modal";

import styles from "./styles.module.scss";

type FullBook = {
  authors: string[];
  category: string;
  description: string;
  id: string;
  imageUrl: string;
  isbn10: string;
  isbn13: string;
  language: string;
  pageCount: number;
  published: number;
  publisher: string;
  title: string;
};

interface BookModalWrapperProps {
  book: FullBook;
  isModalOpen: boolean;
  handleCloseModal;
}

export const BooksModalWrapper = ({
  book,
  isModalOpen,
  handleCloseModal,
}: BookModalWrapperProps) => {
  return (
    <BookModal isOpen={isModalOpen} onRequestClose={handleCloseModal}>
      {book ? (
        <section className={styles.modalContent}>
          <div className={styles.colOne}>
            <img src={book.imageUrl} alt={`${book.title} image`} />
          </div>
          <div className={styles.colTwo}>
            <div className={styles.modalHeader}>
              <h1 className={styles.title}>{book.title}</h1>
              <p>{book.authors?.join(", ")}</p>
            </div>
            <div className={styles.infoWrapper}>
              <b>INFORMAÇÕES</b>
              <span>
                <b>Páginas</b>
                <p>{book.pageCount} páginas</p>
              </span>
              <span>
                <b>Editora</b>
                <p>{book.publisher}</p>
              </span>
              <span>
                <b>Publicação</b>
                <p>{book.published}</p>
              </span>
              <span>
                <b>Idioma</b>
                <p>{book.language}</p>
              </span>
              <span>
                <b>Título original</b>
                <p>{book.title}</p>
              </span>
              <span>
                <b>ISBN-10</b>
                <p>{book.isbn10}</p>
              </span>
              <span>
                <b>ISBN-13</b>
                <p>{book.isbn13}</p>
              </span>
            </div>
            <div className={styles.blockquote}>
              <b>RESENHA DA EDITORA</b>
              <div className={styles.statement}>
                <QuoteIcon />
                <blockquote>{book.description}</blockquote>
              </div>
            </div>
          </div>
        </section>
      ) : (
        <div className={styles.loaderWrapper}>
          <span className={styles.loader}></span>
        </div>
      )}
    </BookModal>
  );
};

const QuoteIcon = () => {
  return (
    <svg
      width="18"
      height="15"
      viewBox="0 0 18 15"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        opacity="0.5"
        d="M17.576 10.712C17.576 8.632 16.172 7.228 14.092 7.228C13.572 7.228 13.156 7.436 13.052 7.488C13.156 5.252 15.028 3.224 17.108 2.86V0C14.04 0.312002 9.62 2.912 9.62 9.412C9.62 12.272 11.388 14.144 13.78 14.144C15.964 14.144 17.576 12.584 17.576 10.712ZM7.956 10.712C7.956 8.632 6.5 7.228 4.472 7.228C3.952 7.228 3.484 7.436 3.38 7.488C3.484 5.252 5.356 3.224 7.488 2.86V0C4.368 0.312002 0 2.912 0 9.412C0 12.272 1.716 14.144 4.16 14.144C6.344 14.144 7.956 12.584 7.956 10.712Z"
        fill="currentColor"
      />
    </svg>
  );
};
