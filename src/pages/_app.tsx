import { QueryClient, QueryClientProvider } from "react-query";
import { AuthProvider } from "../contexts/AuthContext";

import Modal from "react-modal";

import "../styles/globals.scss";

const queryClient = new QueryClient();

Modal.setAppElement("#__next");

function MyApp({ Component, pageProps }) {
  return (
    <QueryClientProvider client={queryClient}>
      <AuthProvider>
        <Component {...pageProps} />
      </AuthProvider>
    </QueryClientProvider>
  );
}

export default MyApp;
