import { GetServerSideProps } from "next";
import Image from "next/image";

import { destroyCookie, parseCookies } from "nookies";
import { useContext, useState } from "react";
import { BookCard } from "../../components/BookContainer";
import { BooksModalWrapper } from "../../components/BooksModalWrapper";
import { Pagination } from "../../components/Pagination";
import { AuthContext } from "../../contexts/AuthContext";
import { getBooks, useBooks } from "../../hooks/useBooks";
import { api } from "../../services/api";

import styles from "./Books.module.scss";

type Book = {
  id: string;
  title: string;
  author: string[];
  pageCount: string;
  publisher: string;
  publishedDate: string;
};

type FullBook = {
  authors: string[];
  category: string;
  description: string;
  id: string;
  imageUrl: string;
  isbn10: string;
  isbn13: string;
  language: string;
  pageCount: number;
  published: number;
  publisher: string;
  title: string;
};

type BookProps = {
  totalCount: number;
  books: Book[];
  user: User;
};

type User = {
  id: string;
  name: string;
  gender: string;
};

export default function Books({ books, totalCount, user }: BookProps) {
  const [page, setPage] = useState(1);
  const [selectedBook, setSelectedBook] = useState({} as FullBook);
  const [isModalOpen, setIsModalOpen] = useState(false);

  const { signOut } = useContext(AuthContext);

  // Handle fetch books

  let cookies = parseCookies(undefined);

  const { "auth.token": token } = cookies;

  const { data, isLoading, isFetching, error } = useBooks(token, page, {
    initialData: books,
  });

  // Handle book Modal

  const handleOpenModal = async (id: string) => {
    setIsModalOpen(!isModalOpen);
    const { data } = await api.get(`books/${id}`, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });

    setSelectedBook(data);
  };

  const handleCloseModal = () => {
    setTimeout(() => {
      setSelectedBook(null);
    }, 500);
    setIsModalOpen(false);
  };

  return (
    <>
      <div className={styles.books}>
        <section className={styles.booksPageWrapper}>
          <header className={styles.header}>
            <div className={styles.logoWrapper}>
              <Image src="/logo_b.svg" height={40} width={198} />
            </div>
            <div className={styles.logoutWrapper}>
              <p>
                Bem vind{user?.gender === "M" ? "o" : "a"}, <b>{user?.name}!</b>
              </p>
              <button type="button" aria-label="Sign out" onClick={signOut}>
                <LogoutIcon />
              </button>
            </div>
          </header>
          <div className={styles.booksWrapper}>
            {isLoading || isFetching ? (
              <div className={styles.loaderWrapper}>
                <span className={styles.loader}></span>
              </div>
            ) : (
              <div className={styles.bookGrid}>
                {data.books?.map((book) => {
                  return (
                    <BookCard
                      key={book.id}
                      id={book.id}
                      title={book.title}
                      authors={book.authors}
                      pageCount={book.pageCount}
                      publishedDate={book.publishedDate}
                      publisher={book.publisher}
                      imgUrl=""
                      onClick={handleOpenModal}
                    />
                  );
                })}
              </div>
            )}

            <Pagination
              currentPage={page}
              totalCount={totalCount}
              onChange={setPage}
            />
          </div>
        </section>
      </div>
      <BooksModalWrapper
        book={selectedBook}
        isModalOpen={isModalOpen}
        handleCloseModal={handleCloseModal}
      />
    </>
  );
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  let cookies = parseCookies(ctx);

  const { "auth.token": token } = cookies;
  const { "user.data": user } = cookies;

  const { books, totalCount, err } = await getBooks(1, token);

  if (err) {
    destroyCookie(undefined, "auth.token");
    destroyCookie(undefined, "auth.refreshToken");

    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }

  const parsedUser = JSON.parse(user);

  return {
    props: {
      books,
      totalCount,
      user: parsedUser,
    },
  };
};

const LogoutIcon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="16"
      height="16"
      viewBox="0 0 16 16"
      fill="none"
    >
      <path
        d="M11 4L15 8M15 8L11 12M15 8L4.5 8"
        stroke="currentColor"
        strokeLinejoin="round"
      />
      <path
        d="M6.5 1H3C1.89543 1 1 1.89543 1 3V13C1 14.1046 1.89543 15 3 15H6.5"
        stroke="currentColor"
        strokeLinejoin="round"
      />
    </svg>
  );
};
