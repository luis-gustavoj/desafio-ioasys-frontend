import { GetServerSideProps } from "next";
import Image from "next/image";
import { parseCookies } from "nookies";
import { FormEvent, useContext, useState } from "react";

import { Input } from "../components/Input";
import { InputWithButton } from "../components/Input/InputWithButton";
import { AuthContext } from "../contexts/AuthContext";

import styles from "./Home.module.scss";

export default function Home() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  const { signIn } = useContext(AuthContext);

  const handleSubmitForm = async (e: FormEvent) => {
    e.preventDefault();

    const error = await signIn({ email, password });
    if (error) {
      setError(error);
    }
  };

  return (
    <main className={styles.home}>
      <section className={styles.homeWrapper}>
        <form onSubmit={handleSubmitForm}>
          <div className={styles.logoWrapper}>
            <Image src="/logo.svg" height={40} width={198} />
          </div>
          <div className={styles.inputsWrapper}>
            <Input
              label="E-mail"
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <InputWithButton
              label="Senha"
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            >
              <button type="submit">Entrar</button>
            </InputWithButton>
          </div>
          <div
            className={`${styles.error} ${
              error !== "" ? styles.visible : styles.invisible
            }`}
          >
            {error}
          </div>
        </form>
      </section>
    </main>
  );
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  let cookies = parseCookies(ctx);

  const { "auth.token": token } = cookies;

  if (token) {
    return {
      redirect: {
        destination: "books",
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
};
