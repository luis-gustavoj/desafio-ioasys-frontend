import Router from "next/router";
import { useQuery, UseQueryOptions } from "react-query";
import { api } from "../services/api";

type Book = {
  id: string;
  title: string;
  authors: string[];
  pageCount: string;
  publisher: string;
  publishedDate: string;
};

type GetBooksResponse = {
  totalCount?: number;
  books?: Book[];
  err?: any;
};

export async function getBooks(
  page: number,
  token: string
): Promise<GetBooksResponse> {
  try {
    const { data } = await api.get(`books?page=${page}&amount=12`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    const totalCount = Math.ceil(data.totalPages);

    const books = data.data.map((book) => {
      return {
        id: book.id,
        title: book.title,
        authors: book.authors,
        pageCount: book.pageCount,
        publisher: book.publisher,
        publishedDate: `Publicado em ${book.published}`,
      };
    });

    const returnObject: GetBooksResponse = {
      books,
      totalCount,
    };

    return returnObject;
  } catch (err) {
    return {
      err,
    };
  }
}

export function useBooks(
  token: string,
  page: number,
  options: UseQueryOptions
) {
  return useQuery(["books", page], () => getBooks(page, token), {
    ...options,
  });
}
